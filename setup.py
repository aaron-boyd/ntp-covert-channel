#!/usr/bin/env python3
from setuptools import setup, find_packages

setup(
    name='ntp-covert-channel',
    version='1.0.0',
    description='Midterm project for secvuln 2021',
    packages=find_packages(),
    install_requires=[
        'tabulate',
        'terminalui @ git+https://gitlab.com/cyberatuc/terminalui/',
    ],
)
