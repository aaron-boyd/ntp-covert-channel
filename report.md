# Security Vulnerability Assessment - Midterm
Aaron Boyd, Clif Wolfe, Chris Morrison

## Assignment
Develop a stealth communication mechanism for sending messages between
computers. It should be impossible or at least extremely difficult for a system
administrator to check whether such messages are actually communicated - either
while a message is being sent or that a message had been sent earlier. It is OK
to use steganography or IP headers to hide messages but:

1. I will be impressed with another approach that has not reached popularity
2. I do not care what the messages actually are - I only care about detecting
   the existence of a message - so checking header values and traffic levels
   should create a challenge for both those approaches
3. I will be really impressed if you use an OS or application vulnerability to
   create the covert channel.

## Technical Approach
For discovering a covert channel, we started with the Network Time Protocol
(NTP). This protocol is widely used in consumer devices and the protocol lacks
many modern security features.

Quickly, Aaron was able to identify the fact the timestamp that the client
transmits is usually random. This means that the client timestamp fields can be
used for broadcasting information to the server. This allows us to encode
hidden messages across these fields, although the messages we can send are
quite small.

## Technical Restrictions of the Method
The largest restrictions of this method are as follows:
- Message size
- Message frequency
- Client initiated
- Server or interception of messages to server required

For message size, the fields used are both uint32 data-types, which gives us a
combined 64 bytes per message, or 32 bytes if we want to limit our message to
the 'fraction of a second' field. This means that the channel will be better
for small data transmission or long transmissions chunked and transmitted over
a long series of messages.

Message frequency is also a factor here. NTP synchronizes between every 1-15
minutes. This means that we will be able to send a maximum of slightly under
3.8 KB per hour if we use both fields and sync every minute or a minimum of 128
bytes per hour if we use the single field and sync once every 15 minutes.

Another restriction of this protocol is that the sync must be client initiated.
So if we want to send a command from our malicious NTP server, we must either
wait until the client syncs again or use a separate channel.

Finally, as NTP is a client-server protocol, we must also be either hosting an
NTP server or intercepting messages to another NTP server in order to recover
the data encoded into the fields. While the method of standing up a new time
server is relatively low effort, the method of interception may be desired for
situations where an extra layer of covertness is required.

Given all of these restrictions, it would make the most sense to use this
method to transmit very small messages infrequently. This could be used in
malware as a 'beacon' method in which the malware communicates periodically to
a c2 service rather than directly such as with a reverse shell.

## Adversarial Analysis
It would be very difficult if not impossible for anyone to detect messages
being sent between a client and server using this channel. The protocol:
1. Initiates a connection from the client ONLY just like NTP
2. Does not alter timestamps in any meaningful or noticeable way
3. Sends packets at packets are normal NTP intervals
4. Has on-the-fly protocol parameter configuration

In the figure below, it can be seen that the NTP server is sending a message
to an NTP client. The client has received 20 bytes of the message.

![Client Receive Message](./images/client-receive-message.png)

In Wireshark, it can be seen the client is sending packets to the server every
60 seconds which is in line with the NTP protocol.

![Client Receive Message Wireshark](./images/client-receive-message-wireshark.png)

In the `covert-ntp-shell`, server NTP packet information can be easily changed.

![Server Config Options](./images/server-config-options.png)

| Field       | Description                                |
| ----------- | ------------------------------------------ |
| `stratum`   | Set Server stratum number                  |
| `ref-id`    | Set reference ID                           |
| `precision` | Set clock precision field log<sub>2</sub>n |

The user can pick a stratum value from one of the options using the
`options stratum` command. In the event they use a stratum value of 1 they can
pick one of the reference IDs from the `options reference_ids` command.

![Stratum and Reference ID Options](images/stratum-ref-id-options.png)

The above configuration would result in the following NTP packet coming from
the server:

![Covert NTP Server Packet](./images/server-wireshark.png)

All of these configuration options makes it VERY hard to signature this
channel.

The control and data fields are encrypted with a 32-bit XOR key which
makes it more difficult to see any plain-text data being sent if a system
administrator where to look at the raw bytes instead of the ASCII timestamp.
One way the key could be extracted is if a system administrator knew exactly
which bytes to look at and knew how the protocol worked. For example, in order
to conform to the NTP protocol, all transactions are initiated by the client.
Therefore, the client must "check in" with the server to see if it has any
messages waiting. The `MESSAGE_CHECK` packet does not send any data and
the data field is therefore all zeroes. When this field is encrypted the XOR
is revealed. Unless someone knew exactly what they were looking for they would
never find this.

```python
KEY = 0x62d92882


class CovertControl(IntEnum):
    UNKNOWN = 0,
    NEW_MESSAGE = 1,
    MESSAGE_CHECK = 2,
    DATA = 3,
    ACK = 4,
    RECV_ACK = 5,
    COMPLETE = 6,
    RESET = 7
```

```
             0123 4567
Covert Data: DDDD.MMLC
Timestamp:   SSSS.FFFF

Covert packet:
    * Data (4-bytes) (D)
    * Message ID (2-bytes) (M)
    * Data Length (1-byte) (L)
    * Control (1-byte) (C)

Timestamp:
    * Seconds (4-bytes) (S)
    * Fraction (4-bytes) (F)
```

When a `MESSAGE_CHECK` packet is sent, the Covert Data will be:
```
             0123 4567
Covert Data: 0000.0001
Timestamp:   0000.0001
```

When each field is encrypted the all zero data field will reveal the key:

```
Covert Data: 0x00000000 0x00000001
Key:         0x62d92882 0x62d92882
Cipher Text: 0x62d92882 0x60d9d77d
```

This key leak can be seen below:

![MESSAGE_CHECK Vulnerability](./images/message-check-vulnerability.png)

## Related Known Techniques
While we did independently discover our method, it appears that the method is
already known. In
[this ACM publication](https://dl.acm.org/doi/pdf/10.1145/3058060.3058082) a
very similar method is detailed that uses the timestamp fields to embed message
data that would otherwise look like correctly used NTP.

Additionally, to assist users of this method who wish to stand their own NTP
server outside of a target network, metasploit contains the module
`auxiliary/scanner/ntp/ntp_monlist` for testing if a specific external address
is allowed for the NTP protocol from a target network.

We were unable to discover any well known existing malware samples making use
of this method. However it does seem that there are more direct abuses of the
NTP protocol. However these are largely attributed to design defects in
commercial products rather than genuine malicious intention.

<div style="page-break-after: always"></div>

## Appendix A: NTP Packet Structure
```c
typedef struct
{
    uint8_t li_vn_mode; // Eight bits. li, vn, and mode.
                        // li.   Two bits.   Leap indicator
                        // vn.   Three bits. Version number of the protocol
                        // mode. Three bits. Client will pick mode 3 for client

    uint8_t stratum;   // Stratum level of the local clock.
    uint8_t poll;      // Maximum interval between successive messages.
    uint8_t precision; // Precision of the local clock.

    uint32_t rootDelay;      // Total round trip delay time.
    uint32_t rootDispersion; // Max error aloud from primary clock source.
    uint32_t refId;          // Reference clock identifier.

    uint32_t refTm_s; // Reference time-stamp seconds.
    uint32_t refTm_f; // Reference time-stamp fraction of a second.

    uint32_t origTm_s; // Originate time-stamp seconds.
    uint32_t origTm_f; // Originate time-stamp fraction of a second.

    uint32_t rxTm_s; // Received time-stamp seconds.
    uint32_t rxTm_f; // Received time-stamp fraction of a second.

    uint32_t txTm_s; // 32 bits and the most important field the client cares
                     // about. Transmit time-stamp seconds.
    uint32_t txTm_f; // 32 bits. Transmit time-stamp fraction of a second.

} ntp_packet; // Total: 384 bits or 48 bytes.
```

<div style="page-break-after: always"></div>

## Appendix B: Covert Protocol NTP Packet Structure
The covert NTP channel protocol uses two 32-bit fields to hide message
information. The first 32-bit field contains message data and the second
contains control information like associated message ID, length of data, and
a control ID (Appendix D).

A single covert NTP packet looks like the following:
```c
struct Packet
{
    uint32_t data;
    uint16_t messageID;
    uint8_t  dataLength:
    uint8_t  control;
};
```

<div style="page-break-after: always"></div>

## Appendix C: NTP Timestamp Structure Between Server-Client
The NTP protocol keeps track of timestamps using two 32-bit fields:
1. Timestamp Seconds
2. Timestamp Seconds Fraction

### Client
The covert NTP channel client uses the "Transmit Timestamp" field of the NTP
protocol to hide its messages.

This choice was made through observing an `openntpd` daemon in Wireshark, it can
be seen when a client sends query to a server that it chooses a random value for
the "Transmit Timestamp". Therefore, one can hide data in this field without
arousing suspicion from a system administrator.

![OpenNTPd Client Query in Wireshark](./images/client-query.png)

### Server
The covert NTP channel server uses the fraction portion of the "Reference
Timestamp" and "Receive Timestamp" of the NTP protocol to hide its messages.

This choice was made because through observing NTP server responses in in
Wireshark and reading the NTP RFC. In order to keep consistent with the covet
Client packets we wanted to hide data in a 32-bit block and control information
in another 32-bit block. NTP timestamps are two 32-bit fields that represent
the times seconds and the times fraction.

While reading the NTP v4 RFC, we noted the meaning of each timestamp:

Reference Timestamp
: Time when the system clock was last set or corrected, in NTP timestamp format

Origin Timestamp (org)
: Time at the client when the request departed for the server, in NTP timestamp
format

Receive Timestamp (rec)
: Time at the server when the request arrived from the client, in NTP timestamp
format

Transmit Timestamp (xmt)
: Time at the server when the response left for the client, in NTP timestamp
format

In Wireshark we noticed that the Transmit Timestamp of the Client became the
Origin Timestamp of the Server's response. In order to fit the NTP protocol and
not arouse any suspicion from system-administrators, we could not hide new
Server messages in the Origin Timestamp.

Since the timestamps on the server in practice are going to be "the real time",
we thought it important to make sure our messages did not alter the timestamps
in a noticeable way. We proceeded under the assumption that a system
administrator would not look further into a timestamp than the seconds portion.
Therefore, hiding data in timestamp fractions are fair game.

For the server packets we found the following fields as valid candidates for
hiding data without raising suspicion:

1. Reference Timestamp Fraction
2. Receive Timestamp Fraction
3. Transmit Timestamp Fraction

![NTP Server Response in Wireshark](./images/server-response.png)

<div style="page-break-after: always"></div>

## Appendix D: Control Data in Covert NTP
The control field of the covert NTP protocol uses the following values:

| Control Name  | Value | Description                      | Data Description |
| ------------- | ----- | -------------------------------- | ---------------- |
| UNKNOWN       | 0     | Unknown control                  |                  |
| NEW_MESSAGE   | 1     | Initiate message sending         | message length   |
| MESSAGE_CHECK | 2     | Check server for messages        |                  |
| DATA          | 3     | Size of message about to be sent | message data     |
| ACK           | 4     | General acknowledgement          | unsigned int     |
| RECV_ACK      | 5     | Indicate received data           | amount received  |
| COMPLETE      | 6     | Complete message send/receive    |                  |
| RESET         | 7     | Drop connection for any reason   | reason           |

<div style="page-break-after: always"></div>

## Appendix E: Sending Exchange in NTP
Since NTP connections are only initiated from the client there are two cases
in which messages will be exchanged:
1. Client tells the server it wants to send a message
2. Client tells the server it wants to receive a message

### Client Sending Message to Server
```
Client            Server
+----------------------+
|     NEW_MESSAGE      |
|--------------------->|
|     ACK              |
|<---------------------|
|     DATA             |
|--------------------->|
|     RECV_ACK         |
|<---------------------|
|     DATA             |
|--------------------->|
|     RECV_ACK         |
|<---------------------|
|                      |
|     ...              |
|                      |
|     DATA             |
|--------------------->|
|     COMPLETE         |
|<---------------------|
V                      V
```

<div style="page-break-after: always"></div>

### Client Receiving Message from Server
```
Client            Server
+----------------------+
|     MESSAGE_CHECK    |
|--------------------->|
|     ACK              |
|<---------------------|
|     RECV_ACK         |
|--------------------->|
|     DATA             |
|<---------------------|
|     RECV_ACK         |
|--------------------->|
|                      |
|     ...              |
|                      |
|     DATA             |
|<---------------------|
|     COMPLETE         |
|--------------------->|
V                      V
```
