#!/bin/bash

find . -iname __pycache__ | xargs rm -r
find . -iname *.pyc | xargs rm