#!/usr/bin/env python3
import colorama

from terminalui.shell import CmdShell as Cmd

ASCII_ART = """
     ___
    / |,\\    ____      __  _  _____ _____ 
   |- ' -|  / (__` == |  \| ||_   _|| ()_)
    \_,_/   \____)    |_|\__|  |_|  |_|
    /___\\
"""

PROMPT = 'c-ntp' + colorama.Fore.GREEN + ' $ ' + colorama.Style.RESET_ALL

main = Cmd(
    # General Settings
    # prompt=" $ ",
    program_name='Covert NTP',  # only needed with "python -m" usage
    command_modules=['commands'],
    prompt=PROMPT,
    ascii_art=ASCII_ART,  # defaults to not printing anything
)

if __name__ == '__main__':
    main()
