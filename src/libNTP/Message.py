from enum import IntEnum


class Status(IntEnum):
    INIT = 0
    PROCESSING = 1,
    SUCCESS = 2,
    ERROR = 3


class Message:
    """
    Container for data being sent from client and server.
    """
    headers = ['status', 'size', 'expected size', 'message']

    def __init__(self, data: bytes, expected_size, status=Status.INIT):
        self.data = data
        self.expected_size = expected_size
        self.status = status

    def __repr__(self):
        return '[{}] {}'.format(self.status.name, self.data.decode())

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        self.num = 0
        self.attrs = [self.status.name, len(self), self.expected_size, self.data]
        return self

    def __next__(self):
        if self.num >= len(Message.headers):
            raise StopIteration()
        item = self.attrs[self.num]
        self.num += 1
        return item

    def add_data(self, new_data: bytes):
        """Add data to message

        :param new_data: bytes to append to end of message
        :return: None
        :raises: OverflowError
        """
        new_size = len(new_data) + len(self)
        if new_size > self.expected_size:
            extra = new_size - self.expected_size
            raise OverflowError('Got {} more bytes than expected ({}).'.format(extra, self.expected_size))
        self.data += new_data

    def get_data(self, start, amount):
        """Get chunk of bytes from message.

        If the amount requested is more than the available
        chunk, this method will return what is available.

        :param start: Beginning index of message to read from
        :param amount: Number of bytes to get
        :return: bytes
        """
        if len(self.data[start:]) < amount:
            return self.data[start:]
        else:
            return self.data[start:start + amount]

    def full(self):
        """Determine if message is expected size

        :return: bool
        """
        return len(self.data) == self.expected_size

    def is_empty(self):
        """Determine if message is empty.

        :return: bool
        """
        return bool(len(self.data))
