from .Message import Message
from .Message import Status as MessageStatus


class MessageBox:
    """
    Object used to keep track of Messages being
    sent to and from server/client.
    """
    def __init__(self, max_size=0):
        self.max_size = max_size
        self.messages = []

    def __len__(self):
        return len(self.messages)

    def __getitem__(self, message_id):
        return self.messages[message_id]

    def __setitem__(self, message_id, new_message):
        self.messages[message_id] = new_message

    def allocate_message(self, expected_size, data=b''):
        """Allocate a message in the message box.

        :param expected_size: Expected size of message
        :param data: Any initial message data
        :return: message ID
        """
        if len(self.messages) >= self.max_size:
            raise OverflowError('Message box is full!')

        next_id = len(self.messages)

        if len(data):
            self.messages.append(Message(data, expected_size, MessageStatus.PROCESSING))
        else:
            self.messages.append(Message(data, expected_size, MessageStatus.INIT))

        return next_id

    def full(self):
        """Determine if message box is full.

        :return: bool
        """
        return len(self.messages) >= self.max_size

    def empty_message_box(self):
        """Empty message box"""
        self.messages = []
