import socket
import threading

from terminalui.tui import log_error
from terminalui.tui import log_normal
from time import sleep

from .CovertNTP import ClientNTPPacket
from .CovertNTP import CovertControl
from .CovertNTP import CovertError
from .CovertNTP import CovertInterface
from .CovertNTP import CovertNTPPacket
from .CovertNTP import KEY
from .Message import Message
from .Message import Status as MessageStatus
from .MessageBox import MessageBox


class ClientThread(threading.Thread):

    def __init__(self, message_box, hostname, port, poll):
        threading.Thread.__init__(self)
        self._running = True
        self.hostname = hostname
        self.port = port
        self.poll = poll
        self.message_box = message_box

    def stop(self):
        self._running = False

    def send_and_receive_packet(self, out_packet):
        """Send and receive a single packet to and from server.

        :param out_packet: packet to send to server
        :return: Response NTP packet
        """
        sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        sock.settimeout(2)
        data = out_packet.get_encrypted_bytes(KEY)
        sock.sendto(data, (self.hostname, self.port))
        received = sock.recv(1024)
        in_packet = CovertNTPPacket.from_encrypted_bytes(received, KEY)
        sleep(self.poll)
        return in_packet


class ClientSendThread(ClientThread):

    def __init__(self, inbox, hostname, port, poll):
        super().__init__(inbox, hostname, port, poll)
        self._sending = True

    def run(self):
        """Send any all messages belonging to this thread"""
        for message in self.message_box:
            if message.status == MessageStatus.INIT:
                self.send_message(message)

            if not self._running:  # Thread has been told to stop prematurely
                break

    def send_message(self, message):
        """Send a message to an NTP server

        :param message: Message to send
        :return: None
        """
        message.status = MessageStatus.PROCESSING
        try:
            self.send_message_bytes(message.data)
            message.status = MessageStatus.SUCCESS
        except socket.timeout:
            pass
        except Exception as e:
            message.status = MessageStatus.ERROR
            log_error(str(e))

    def send_message_bytes(self, data: bytes):
        """Break message up and send it to NTP server

        :param data: Message bytes
        :return: None
        :raises: Exception on failure to send
        """
        chunk_size = CovertNTPPacket.MAX_DATA_LEN
        # Get ID of message server side
        ret, message_id = self.get_new_message_id(len(data))
        if ret != CovertControl.ACK:
            raise Exception('Failed to get new message id: {}'.format(ret.name))

        def bail(error):
            raise Exception('Failed to send message {}: {}'.format(message_id, error.name))

        ret = CovertControl.UNKNOWN
        for i in range(0, len(data), chunk_size):
            start = i
            end = i + chunk_size
            data_packet = ClientNTPPacket(CovertControl.DATA, message_id=message_id, data=data[start:end])
            resp_packet = self.send_and_receive_packet(data_packet)
            ret = resp_packet.control
            if ret not in [CovertControl.RECV_ACK, CovertControl.COMPLETE]:
                error = CovertError(resp_packet.int_data) or CovertError.UNKNOWN
                bail(error)

            # Thread has been told to stop prematurely and message was not finished sending
            if not self._running and ret != CovertControl.COMPLETE:
                error = CovertError.INTERRUPTED
                bail(error)

        # Control should be COMPLETE on exit of loop indicating all of message got there
        if ret != CovertControl.COMPLETE:
            error = CovertError.PARTIAL_MESSAGE
            bail(error)

    def get_new_message_id(self, message_len):
        """Tell server client wants to send a message.

        :param message_len: Length of message to send
        :return: ID of message server side
        """
        new_message_packet = ClientNTPPacket(CovertControl.NEW_MESSAGE, data=message_len)
        resp_packet = self.send_and_receive_packet(new_message_packet)
        return resp_packet.control, resp_packet.message_id


class ClientReceiveThread(ClientThread):

    def __init__(self, inbox, hostname, port, poll):
        super().__init__(inbox, hostname, port, poll)
        self._receiving = True

    def run(self):
        """Loop and check server for messages"""
        while self._running:
            try:
                new_message_id = self.check_for_message()
                if new_message_id >= 0:
                    self.receive_message_bytes(new_message_id)
            except socket.timeout:
                pass
            except Exception as e:
                log_error(str(e))

    def check_for_message(self):
        """Check in with server and get size of next available message

        :returns: ID of of new message client side
        """
        message_check_packet = ClientNTPPacket(CovertControl.MESSAGE_CHECK)
        resp_packet = self.send_and_receive_packet(message_check_packet)
        if resp_packet.control != CovertControl.ACK:  # No messages
            return -1

        expected_size = resp_packet.int_data
        message_id = self.message_box.allocate_message(expected_size)

        return message_id

    def receive_message_bytes(self, message_id):
        """Get message data from server

        :param message_id: ID of message client side
        :return: None
        """
        # Let server know we have received 0 bytes of the message
        curr_pos = 0
        recv_ack_packet = ClientNTPPacket(CovertControl.RECV_ACK, message_id=message_id, data=curr_pos)
        resp_packet = self.send_and_receive_packet(recv_ack_packet)

        def bail(error):
            raise Exception('Failed to receive message {}: {}'.format(message_id, error.name))

        # Loop until server stops sending data
        while resp_packet.control == CovertControl.DATA:
            self.message_box[message_id].add_data(resp_packet.data)  # add received data to message
            # Update position to let server know how much client has received
            curr_pos = len(self.message_box[message_id])
            recv_ack_packet = ClientNTPPacket(CovertControl.RECV_ACK, message_id=message_id, data=curr_pos)
            resp_packet = self.send_and_receive_packet(recv_ack_packet)

            # Thread has been told to stop prematurely and message was not finished sending
            if not self._running and resp_packet.control != CovertControl.COMPLETE:
                self.message_box[message_id].status = MessageStatus.ERROR
                bail(CovertError.INTERRUPTED)

        if resp_packet.control == CovertControl.COMPLETE:
            self.message_box[message_id].status = MessageStatus.SUCCESS
        else:
            if resp_packet.control == CovertControl.RESET:
                error = CovertError(recv_ack_packet.int_data)
            else:
                error = CovertError.UNKNOWN
            self.message_box[message_id].status = MessageStatus.ERROR
            bail(error)


class CovertNTPClient(CovertInterface):

    def __init__(self):
        super().__init__()
        self.send_thread = None
        self.receive_thread = None

    def shutdown(self):
        self.stop_send()
        self.stop_receive()

    def start_send(self):
        """Start thread to send all messages in outbox"""
        if not self.send_thread:
            self.send_thread = ClientSendThread(self.outbox, self.hostname, self.port, self.poll)
            self.send_thread.start()

    def stop_send(self):
        """Stop thread that is sending all messages in outbox"""
        if self.send_thread:
            self.send_thread.stop()
            self.send_thread.join()
            self.send_thread = None

    def start_receive(self):
        """Start thread to receive messages into inbox"""
        if not self.receive_thread:
            self.receive_thread = ClientReceiveThread(self.inbox, self.hostname, self.port, self.poll)
            self.receive_thread.start()

    def stop_receive(self):
        """Start thread that is receiving messages into inbox"""
        if self.receive_thread:
            self.receive_thread.stop()
            self.receive_thread.join()
            self.receive_thread = None

    def add_to_outbox(self, message: str):
        msg_bytes = message.encode()
        self.outbox.messages.append(Message(msg_bytes, len(msg_bytes)))

    def clear_outbox(self):
        if self.send_thread:
            raise Exception('Cannot clear outbox while messages are being sent.')
        self.outbox = MessageBox(CovertNTPClient.MESSAGE_BOX_SIZE)

    def clear_inbox(self):
        if self.receive_thread:
            raise Exception('Cannot clear outbox while messages are being received.')
        self.inbox = MessageBox(CovertNTPClient.MESSAGE_BOX_SIZE)

    def is_sending(self):
        if self.send_thread:
            return True
        else:
            return False

    def is_receiving(self):
        if self.receive_thread:
            return True
        else:
            return False

    def update_settings(self, settings):
        try:
            self.hostname = settings['hostname']
            self.port = settings['port']
            self.poll = 2 ** settings['poll']
        except KeyError:
            raise Exception('Make sure "hostname", "port", and "poll" are set.')


def main():
    import argparse
    from commands.globals import shared_settings
    from commands.globals import get_interface

    parser = argparse.ArgumentParser(description='Send message to NTP server')
    parser.add_argument('-n',
                        '--hostname',
                        default='localhost',
                        help='Hostname of NTP server')

    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=123,
                        help='Port to connect to on NTP server')

    parser.add_argument('-l',
                        '--poll',
                        type=int,
                        default=1,
                        help='Interval to send packets')

    parser.add_argument('message',
                        help='Message to send to server')

    args = parser.parse_args()

    shared_settings['mode'] = 'client'
    shared_settings['hostname'] = args.hostname
    shared_settings['port'] = args.port
    shared_settings['poll'] = args.poll
    client = get_interface()

    client.add_to_outbox(args.message)

    messages = [
        'Having margaret means shyness most.',
        'Described sense age right among mistress.',
        'Unlocked forbade humanity extensive improve painful mirth tended.'
    ]
    for message in messages:
        client.add_to_outbox(message)

    try:
        client.start_send()
        client.start_receive()
    except KeyboardInterrupt:
        log_normal('Shutting down...')
    finally:
        client.stop_send()
        client.stop_receive()


if __name__ == '__main__':
    main()
