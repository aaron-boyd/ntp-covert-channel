import struct
import time

from enum import IntEnum

NTP_PORT = 123

# Number of seconds between Jan 1 1900 and Jan 1 1970
EPOCH_1900 = 2208988800


def get_ntp_epoch():
    return round(time.time()) + EPOCH_1900


class Mode(IntEnum):
    RESERVED = 0,
    SYMMETRIC_ACTIVE = 1,
    SYMMETRIC_PASSIVE = 2,
    CLIENT = 3,
    SERVER = 4,
    BROADCAST = 5,
    NTP_CONTROL_MESSAGE = 6


class Version(IntEnum):
    V1 = 1,
    V2 = 2,
    V3 = 3,
    V4 = 4


class LeapIndicator(IntEnum):
    NO_WARNING = 0,  # no warning
    SECONDS_61 = 1,  # last minute of day has 61 seconds
    SECONDS_59 = 2,  # last minute of day has 59 seconds
    UNKNOWN = 3  # unknown (clock unsynchronized)


class ReferenceIdentifiers(IntEnum):
    GOES = int(b'GOES'.hex(), 16),  # Geosynchronous Orbit Environment Satellite
    GPS = int(b'GPS\x00'.hex(), 16),  # Global Position System
    GAL = int(b'GAL\x00'.hex(), 16),  # Galileo Positioning System
    PPS = int(b'PPS\x00'.hex(), 16),  # Generic pulse-per-second
    IRIG = int(b'IRIG'.hex(), 16),  # Inter-Range Instrumentation Group
    WWVB = int(b'WWVB'.hex(), 16),  # LF Radio WWVB Ft. Collins, CO 60 kHz
    DCF = int(b'DCF\x00'.hex(), 16),  # LF Radio DCF77 Mainflingen, DE 77.5 kHz
    HBG = int(b'HBG\x00'.hex(), 16),  # LF Radio HBG Prangins, HB 75 kHz
    MSF = int(b'MSF\x00'.hex(), 16),  # LF Radio MSF Anthorn, UK 60 kHz
    JJY = int(b'JJY\x00'.hex(), 16),  # LF Radio JJY Fukushima, JP 40 kHz, Saga, JP 60 kHz
    LORC = int(b'LORC'.hex(), 16),  # MF Radio LORAN C station, 100 kHz
    TDF = int(b'TDF\x00'.hex(), 16),  # MF Radio Allouis, FR 162 kHz
    CHU = int(b'CHU\x00'.hex(), 16),  # HF Radio CHU Ottawa, Ontario
    WWV = int(b'WWV\x00'.hex(), 16),  # HF Radio WWV Ft. Collins, CO
    WWVH = int(b'WWVH'.hex(), 16),  # HF Radio WWVH Kauai, HI
    NIST = int(b'NIST'.hex(), 16),  # NIST telephone modem
    ACTS = int(b'ACTS'.hex(), 16),  # NIST telephone modem
    USNO = int(b'USNO'.hex(), 16),  # USNO telephone modem
    PTB = int(b'PTB\x00'.hex(), 16)  # European telephone modem


class Packet:
    """
    Class used to follow the NTP protocol

    Spec: https://tools.ietf.org/html/rfc5905

    Reference Timestamp: Time when the system clock was last set or
    corrected, in NTP timestamp format.

    Origin Timestamp (org): Time at the client when the request departed
    for the server, in NTP timestamp format.

    Receive Timestamp (rec): Time at the server when the request arrived
    from the client, in NTP timestamp format.

    Transmit Timestamp (xmt): Time at the server when the response left
    for the client, in NTP timestamp format.
    """
    pack_str = '!BBbbIIIIIIIIIII'

    def __init__(self, *args, **kwargs):
        self.li_vn_mode = kwargs.get('li_vn_mode') or 0  # 8-bits
        self.stratum = kwargs.get('stratum') or 0  # 8-bits
        self.poll = kwargs.get('poll') or 0  # 8-bits
        self.precision = kwargs.get('precision') or 0  # 8-bits
        self.root_delay = kwargs.get('root_delay') or 0  # 32-bits
        self.root_dispersion = kwargs.get('root_dispersion') or 0  # 32-bits
        self.ref_id = kwargs.get('ref_id') or 0  # 32-bits
        self.ref_time_s = kwargs.get('ref_time_s') or 0  # 32-bits
        self.ref_time_f = kwargs.get('ref_time_f') or 0  # 32-bits
        self.orig_time_s = kwargs.get('orig_time_s') or 0  # 32-bits
        self.orig_time_f = kwargs.get('orig_time_f') or 0  # 32-bits
        self.rx_time_s = kwargs.get('rx_time_s') or 0  # 32-bits
        self.rx_time_f = kwargs.get('rx_time_f') or 0  # 32-bits
        self.tx_time_s = kwargs.get('tx_time_s') or 0  # 32-bits
        self.tx_time_f = kwargs.get('tx_time_f') or 0  # 32-bits

    def __bytes__(self):
        return struct.pack(self.pack_str,
                           self.li_vn_mode,
                           self.stratum,
                           self.poll,
                           self.precision,
                           self.root_delay,
                           self.root_dispersion,
                           self.ref_id,
                           self.ref_time_s,
                           self.ref_time_f,
                           self.orig_time_s,
                           self.orig_time_f,
                           self.rx_time_s,
                           self.rx_time_f,
                           self.tx_time_s,
                           self.tx_time_f)

    def from_bytes(self, data):
        (self.li_vn_mode,
         self.stratum,
         self.poll,
         self.precision,
         self.root_delay,
         self.root_dispersion,
         self.ref_id,
         self.ref_time_s,
         self.ref_time_f,
         self.orig_time_s,
         self.orig_time_f,
         self.rx_time_s,
         self.rx_time_f,
         self.tx_time_s,
         self.tx_time_f) = struct.unpack(self.pack_str, data)

    @property
    def mode(self):
        """Get NTP Mode of packet"""
        mode = self.li_vn_mode & 0b00000111
        for ntp_mode in Mode:
            if mode == ntp_mode.value:
                return ntp_mode
        raise KeyError('Invalid NTP Protocol mode {}'.format(mode))  # Should never get here

    @property
    def leap_indicator(self):
        """Get Leap Indicator of packet"""
        ln = (self.li_vn_mode & 0b11000000) >> 6
        for indicator in LeapIndicator:
            if ln == indicator.value:
                return indicator
        raise KeyError('Invalid NTP Leap Indicator {}'.format(ln))  # Should never get here

    @property
    def version(self):
        """Get NTP version of packet"""
        version = (self.li_vn_mode & 0b00111000) >> 3
        for ntp_version in Version:
            if version == ntp_version.value:
                return ntp_version
        raise KeyError('Invalid NTP Version {}'.format(version))  # Should never get here

    @mode.setter
    def mode(self, value: Mode):
        """Set NTP mode of packet"""
        for mode in Mode:
            if value == mode.value:
                self.li_vn_mode = (self.leap_indicator << 6) + (self.version << 3) + value
                break

    @leap_indicator.setter
    def leap_indicator(self, value: LeapIndicator):
        """Set Leap Indicator of packet"""
        for indicator in LeapIndicator:
            if value == indicator.value:
                self.li_vn_mode = (value << 6) + (self.version << 3) + self.mode
                break

    @version.setter
    def version(self, value: Version):
        """Set NTP Version of packet"""
        for ntp_version in Version:
            if value == ntp_version.value:
                self.li_vn_mode = (self.leap_indicator << 6) + (value << 3) + self.mode
                break
