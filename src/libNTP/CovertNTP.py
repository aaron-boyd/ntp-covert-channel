import random
import struct

from enum import IntEnum
from terminalui.tui import log_normal

from .Message import Message
from .MessageBox import MessageBox
from .NTP import Mode as NTPMode
from .NTP import Packet as NTPPacket
from .NTP import Version as NTPVersion
from .NTP import get_ntp_epoch

KEY = 0x62d92882  # Maybe global config objet


class CovertControl(IntEnum):
    UNKNOWN = 0,
    NEW_MESSAGE = 1,
    MESSAGE_CHECK = 2,
    DATA = 3,
    ACK = 4,
    RECV_ACK = 5,
    COMPLETE = 6,
    RESET = 7


class CovertError(IntEnum):
    UNKNOWN = 1,
    NO_MESSAGES = 2,
    BOX_FULL = 3,
    MESSAGE_OVERFLOW = 4,
    INTERRUPTED = 5,
    PARTIAL_MESSAGE = 6


class CovertNTPPacket(NTPPacket):
    """
    The covert NTP packets are hidden in the seconds and fraction
    field of a timestamp depending on which Mode the packet is in.

                 0123 4567
    Covert Data: DDDD.MMLC
    Timestamp:   SSSS.FFFF

    Covert packet:
      * Data (4-bytes) (D)
      * Message ID (2-bytes) (M)
      * Data Length (1-byte) (L)
      * Control (1-byte) (C)

    Timestamp:
      * Seconds (4-bytes) (S)
      * Fraction (4-bytes) (F)
    """
    # Total area to hide message
    MAX_HIDING_SIZE = 8
    MESSAGE_ID_PACK_STR = 'H'
    # Message ID size is constrained by the size of H (2-bytes)
    MAX_MESSAGE_ID = 2 ** (struct.calcsize(MESSAGE_ID_PACK_STR) * 8) - 1
    DATA_LEN_PACK_STR = 'B'
    CTRL_PACK_STR = 'B'
    META_PACK_STR = MESSAGE_ID_PACK_STR + DATA_LEN_PACK_STR + CTRL_PACK_STR
    MAX_DATA_LEN = MAX_HIDING_SIZE - struct.calcsize(META_PACK_STR)
    DATA_PACK_STR = '={}s'.format(MAX_DATA_LEN)

    COVERT_PACK_STR = DATA_PACK_STR + META_PACK_STR
    # 2 unsigned 4-byte ints
    SEC_FRAC_PACK_STR = 'II'
    HIDDEN_FIELD_1 = None
    HIDDEN_FIELD_2 = None

    def __init__(self, control: CovertControl, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.control = control
        self.message_id = kwargs['message_id'] if 'message_id' in kwargs else self.MAX_MESSAGE_ID
        self.version = kwargs['ntp_version'] if 'ntp_version' in kwargs else NTPVersion.V4
        self.data = kwargs['data'] if 'data' in kwargs else None
        seconds, fraction = CovertNTPPacket.pack_hdr_and_data(self.control, self.message_id, self.data)
        self.__setattr__(self.HIDDEN_FIELD_1, seconds)
        self.__setattr__(self.HIDDEN_FIELD_2, fraction)

    def __repr__(self):
        fmt_str = '[{}] MSG_ID={} DATA={}'
        try:
            decoded_data = self.data.decode()
        except UnicodeDecodeError:
            decoded_data = self.data

        message = fmt_str.format(self.control.name, self.message_id, decoded_data)
        return message

    @property
    def message_id(self):
        _, message_id, _, _ = self.get_packed_hidden_data()
        return message_id

    @property
    def data(self):
        data, _, _, _ = self.get_packed_hidden_data()
        return data[:self.data_length]

    @property
    def int_data(self):
        num = struct.unpack('I', self.data)[0]
        return num

    @property
    def data_length(self):
        _, _, data_length, _ = self.get_packed_hidden_data()
        return data_length

    @property
    def control(self):
        _, _, _, ctrl = self.get_packed_hidden_data()
        return ctrl

    @data.setter
    def data(self, data):
        data, data_length = self.data_to_bytes(data)
        seconds, fraction = CovertNTPPacket.pack_hdr_and_data(self.control, self.message_id, data)
        self.__setattr__(self.HIDDEN_FIELD_1, seconds)
        self.__setattr__(self.HIDDEN_FIELD_2, fraction)

    @control.setter
    def control(self, ctrl: CovertControl):
        seconds, fraction = CovertNTPPacket.pack_hdr_and_data(ctrl, self.message_id, self.data)
        self.__setattr__(self.HIDDEN_FIELD_1, seconds)
        self.__setattr__(self.HIDDEN_FIELD_2, fraction)

    @message_id.setter
    def message_id(self, message_id):
        seconds, fraction = CovertNTPPacket.pack_hdr_and_data(self.control, message_id, self.data)
        self.__setattr__(self.HIDDEN_FIELD_1, seconds)
        self.__setattr__(self.HIDDEN_FIELD_2, fraction)

    def get_packed_hidden_data(self):
        """Get covert NTP data from fields it is hiding in.

        :return: data bytes, message id, data length, covert control
        """
        seconds = self.__getattribute__(self.HIDDEN_FIELD_1)
        fraction = self.__getattribute__(self.HIDDEN_FIELD_2)
        packed_data = struct.pack(self.SEC_FRAC_PACK_STR, seconds, fraction)
        data, message_id, data_length, ctrl = struct.unpack(self.COVERT_PACK_STR, packed_data)
        return data, message_id, data_length, CovertControl(ctrl)

    @classmethod
    def pack_hdr_and_data(cls, control, message_id, data):
        """Pack header and data into hidden fields.

        :param control: CovertControl enum indicating what packet is for
        :param message_id: ID of message that packet corresponds to
        :param data: Any data bytes
        :return: tuple with two 32-bit integers
        """
        data_bytes, data_length = cls.data_to_bytes(data)
        packed_data = struct.pack(CovertNTPPacket.COVERT_PACK_STR, data_bytes, message_id, data_length, control)
        seconds, fraction = struct.unpack(CovertNTPPacket.SEC_FRAC_PACK_STR, packed_data)
        return seconds, fraction

    @classmethod
    def data_to_bytes(cls, data):
        """Convert packet data to bytes object.

        This will convert an int or bytes to a bytes object
        of length MAX_DATA_LEN (usually 4).

        :param data: data to be converted
        :return: bytes
        """
        if isinstance(data, int):
            data_bytes = data.to_bytes(cls.MAX_DATA_LEN, 'little')
            data_length = cls.MAX_DATA_LEN
        elif isinstance(data, bytes):
            data_bytes = data
            data_length = len(data)
            if data_length > cls.MAX_DATA_LEN:
                raise OverflowError('Data too long! Wanted {}, got {}.'.format(cls.MAX_DATA_LEN, data_length))
        else:
            data_bytes = b''
            data_length = 0

        return data_bytes, data_length

    @staticmethod
    def from_encrypted_bytes(data, key: int):
        """Take packet bytes and decrypt them.

        :param data: packet bytes
        :param key: 32-bit XOR key
        :return: ClientNTPPacket or ServerNTPPacket
        """
        assert key.bit_length() <= 32
        tmp_packet = NTPPacket()
        tmp_packet.from_bytes(data)

        # Determine what type of packet we are dealing with
        if tmp_packet.mode == NTPMode.CLIENT:
            packet_type = ClientNTPPacket
        elif tmp_packet.mode == NTPMode.SERVER:
            packet_type = ServerNTPPacket
        else:  # Only support Client and Server mode for now
            raise TypeError('Unsupported Packet Mode: {}'.format(tmp_packet.mode))

        # Get NTP fields that data is hiding in and decrypt it
        seconds = tmp_packet.__getattribute__(packet_type.HIDDEN_FIELD_1) ^ key
        fraction = tmp_packet.__getattribute__(packet_type.HIDDEN_FIELD_2) ^ key

        # Convert to bytes
        packed_data = struct.pack(CovertNTPPacket.SEC_FRAC_PACK_STR, seconds, fraction)
        # Unpack data into covert NTP fields
        data, message_id, data_length, ctrl = struct.unpack(CovertNTPPacket.COVERT_PACK_STR, packed_data)
        # Chop off null-bytes
        data = data[:data_length]
        # Create packet
        packet = packet_type(CovertControl(ctrl), message_id=message_id, data=data)

        return packet


class ClientNTPPacket(CovertNTPPacket):
    """
    Packet class that used by the client to covertly
    send messages to NTP server. It hides its data in
    the tx_time timestamp field of the NTP protocol.
    """
    HIDDEN_FIELD_1 = 'tx_time_s'
    HIDDEN_FIELD_2 = 'tx_time_f'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mode = NTPMode.CLIENT

    def get_encrypted_bytes(self, key: int):
        """Encrypt this packet with a 32-bit XOR key

        :param key: 32-bit XOR key
        :return: bytes object
        """
        assert key.bit_length() <= 32
        return struct.pack(self.pack_str,
                           self.li_vn_mode,
                           self.stratum,
                           self.poll,
                           self.precision,
                           self.root_delay,
                           self.root_dispersion,
                           self.ref_id,
                           self.ref_time_s,
                           self.ref_time_f,
                           self.orig_time_s,
                           self.orig_time_f,
                           self.rx_time_s,
                           self.rx_time_f,
                           self.tx_time_s ^ key,
                           self.tx_time_f ^ key)


class ServerNTPPacket(CovertNTPPacket):
    """
    Packet class that used by the server to covertly
    send messages to NTP client. It hides its data in
    the orig_time timestamp field of the NTP protocol.
    """
    HIDDEN_FIELD_1 = 'ref_time_f'
    HIDDEN_FIELD_2 = 'rx_time_f'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mode = NTPMode.SERVER

    def get_encrypted_bytes(self, key: int):
        """Encrypt this packet with a 32-bit XOR key

        :param key: 32-bit XOR key
        :return: bytes object
        """
        assert key.bit_length() <= 32
        # TODO: Maybe add this to a CovertNTPServer config
        self.root_delay = random.randint(0, 2 ** 12 - 1)
        self.root_dispersion = random.randint(0, 2 ** 12 - 1)
        self.ref_time_s = get_ntp_epoch()
        self.rx_time_s = get_ntp_epoch()
        self.tx_time_s = get_ntp_epoch()
        self.tx_time_f = random.randint(0, 2 ** 32 - 1)
        return struct.pack(self.pack_str,
                           self.li_vn_mode,
                           self.stratum,
                           self.poll,
                           self.precision,
                           self.root_delay,
                           self.root_dispersion,
                           self.ref_id,
                           self.ref_time_s,
                           self.ref_time_f ^ key,
                           self.orig_time_s,
                           self.orig_time_f,
                           self.rx_time_s,
                           self.rx_time_f ^ key,
                           self.tx_time_s,
                           self.tx_time_f)


class CovertInterface:
    MESSAGE_BOX_SIZE = CovertNTPPacket.MAX_MESSAGE_ID - 1

    def __init__(self):
        self.hostname = None
        self.port = None
        self.poll = None
        self.inbox = MessageBox(CovertInterface.MESSAGE_BOX_SIZE)
        self.outbox = MessageBox(CovertInterface.MESSAGE_BOX_SIZE)

    def add_to_outbox(self, message: str):
        msg_bytes = message.encode()
        self.outbox.messages.append(Message(msg_bytes, len(msg_bytes)))

    def is_sending(self):
        return False

    def is_receiving(self):
        return False

    def is_running(self):
        return self.is_sending() or self.is_receiving()


def main():
    client_packet = ClientNTPPacket(CovertControl.NEW_MESSAGE, 0x414243444546)
    b1 = client_packet.get_encrypted_bytes(KEY)
    decrypted_client_packet = CovertNTPPacket.from_encrypted_bytes(b1, KEY)
    log_normal('=== Client Packet ===')
    log_normal('Original: {}'.format(client_packet))
    log_normal('Decrypted: {}'.format(decrypted_client_packet))

    server_packet = ClientNTPPacket(CovertControl.ACK, b'GHIJKL')
    b2 = server_packet.get_encrypted_bytes(KEY)
    decrypted_server_packet = CovertNTPPacket.from_encrypted_bytes(b2, KEY)
    log_normal('=== Server Packet ===')
    log_normal('Original: {}'.format(server_packet))
    log_normal('Decrypted: {}'.format(decrypted_server_packet))


if __name__ == '__main__':
    main()
