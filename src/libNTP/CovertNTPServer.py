import socket
import threading

from terminalui.tui import log_error
from terminalui.tui import log_normal

from .CovertNTP import CovertControl
from .CovertNTP import CovertError
from .CovertNTP import CovertInterface
from .CovertNTP import CovertNTPPacket
from .CovertNTP import KEY
from .CovertNTP import ServerNTPPacket
from .Message import Message
from .Message import Status as MessageStatus
from .MessageBox import MessageBox
from .NTP import ReferenceIdentifiers


class ServerThread(threading.Thread):

    def __init__(self, inbox, outbox, hostname, port, stratum, precision, ref_id):
        threading.Thread.__init__(self)
        self._running = True
        self.udp_socket = None
        self.hostname = hostname
        self.port = port
        self.stratum = stratum
        self.precision = precision
        self.ref_id = ref_id
        self.inbox = inbox
        self.outbox = outbox

    def run(self):
        """Sit and loop waiting for connections from client"""
        self.udp_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        self.udp_socket.settimeout(2)
        try:
            self.udp_socket.bind((self.hostname, self.port))
        except PermissionError:
            log_error('Run shell as root.')
            return

        try:
            while self._running:
                try:
                    self.receive_and_send_packet()
                except socket.timeout:
                    pass
        finally:
            self.udp_socket.close()

    def stop(self):
        self._running = False

    def receive_and_send_packet(self):
        """Receive and process a packet from a client"""
        data, address = self.udp_socket.recvfrom(1024)
        in_packet = CovertNTPPacket.from_encrypted_bytes(data, KEY)
        out_packet = self.process_packet(in_packet)
        out_packet.orig_time_s = in_packet.tx_time_s ^ KEY
        out_packet.orig_time_f = in_packet.tx_time_f ^ KEY
        out_packet.stratum = self.stratum
        out_packet.ref_id = self.ref_id
        out_packet.poll = in_packet.poll
        out_packet.precision = self.precision
        out_packet.ref_id = self.ref_id
        out_data = out_packet.get_encrypted_bytes(KEY)
        self.udp_socket.sendto(out_data, address)

    def process_packet(self, client_packet):
        """Process a packet received from client

        :param client_packet: packet received from client
        :return: response packet
        """
        response_packet = ServerNTPPacket(CovertControl.RESET, data=CovertError.UNKNOWN)
        if client_packet.control == CovertControl.NEW_MESSAGE:  # Client wants to send new message
            response_packet = self.get_new_message_response(client_packet)

        elif client_packet.control == CovertControl.DATA:  # Client sent data
            response_packet = self.get_data_response(client_packet)

        elif client_packet.control == CovertControl.MESSAGE_CHECK:  # Client checking for messages
            response_packet = self.get_message_check_response()

        elif client_packet.control == CovertControl.RECV_ACK:  # Client acknowledging received data
            response_packet = self.get_recv_ack_response(client_packet)

        elif client_packet.control == CovertControl.RESET:  # Client error
            response_packet = ServerNTPPacket(CovertControl.ACK)

        return response_packet

    def get_new_message_response(self, client_packet):
        """Get response to clients wish to send a message

        :param client_packet: packet from client
        :return: response packet containing new message ID or error if message box is full
        """
        expected_size = client_packet.int_data
        try:
            new_message_id = self.inbox.allocate_message(expected_size)
            response_packet = ServerNTPPacket(CovertControl.ACK, message_id=new_message_id)
        except OverflowError:
            response_packet = ServerNTPPacket(CovertControl.RESET, data=CovertError.BOX_FULL)
        return response_packet

    def get_data_response(self, client_packet):
        """Get response to data received from client

        :param client_packet: client packet
        :return: response packet acknowledging data or an error
        """
        message_id = client_packet.message_id
        message_data = client_packet.data
        try:
            self.inbox[message_id].add_data(message_data)
            if self.inbox[message_id].full():
                self.inbox[message_id].status = MessageStatus.SUCCESS
                response_packet = ServerNTPPacket(CovertControl.COMPLETE)
            else:
                self.inbox[message_id].status = MessageStatus.PROCESSING
                response_packet = ServerNTPPacket(CovertControl.RECV_ACK)

        except OverflowError:
            response_packet = ServerNTPPacket(CovertControl.RESET, data=CovertError.message_OVERFLOW)
        return response_packet

    def get_message_check_response(self):
        """Finds first available message for client.

        :return: response packet containing length of message or reset if no messages
        """
        message_id = -1
        for i, message in enumerate(self.outbox):
            if message.status not in [MessageStatus.SUCCESS, MessageStatus.PROCESSING]:
                message_id = i
                break

        if message_id != -1:
            message_len = len(self.outbox[message_id])
            response_packet = ServerNTPPacket(CovertControl.ACK, data=message_len)
        else:
            response_packet = ServerNTPPacket(CovertControl.RESET, data=CovertError.NO_MESSAGES)

        return response_packet

    def get_recv_ack_response(self, client_packet):
        """Respond to client acknowledgement of receiving data

        :param client_packet: client packet
        :return: response packet with more message data or COMPLETE
        """
        message_id = client_packet.message_id
        client_cur_pos = client_packet.int_data
        chunk_size = CovertNTPPacket.MAX_DATA_LEN
        data = self.outbox[message_id].get_data(client_cur_pos, chunk_size)
        status = MessageStatus.PROCESSING
        if data:
            response_packet = ServerNTPPacket(CovertControl.DATA, message_id=message_id, data=data)
        else:
            response_packet = ServerNTPPacket(CovertControl.COMPLETE, message_id=message_id, data=0)
            status = MessageStatus.SUCCESS

        self.outbox[message_id].status = status

        return response_packet


class CovertNTPServer(CovertInterface):

    def __init__(self):
        super().__init__()
        self.runner_thread = None
        self.stratum = None
        self.precision = 0
        self.ref_id = None

    def shutdown(self):
        self.stop()

    def start_send(self):
        """Wrapper for start to keep interface consistent with client"""
        self.start()

    def stop_send(self):
        """Wrapper for stop to keep interface consistent with client"""
        self.stop()

    def start_receive(self):
        """Wrapper for start to keep interface consistent with client"""
        self.start()

    def stop_receive(self):
        """Wrapper for stop to keep interface consistent with client"""
        self.stop()

    def start(self):
        """Start the server thread that handles network connections"""
        if not self.runner_thread:
            self.runner_thread = ServerThread(self.inbox, self.outbox, self.hostname, self.port,
                                              self.stratum, self.precision, self.ref_id)
            self.runner_thread.start()

    def stop(self):
        """Stop the server thread that handles network connections"""
        if self.runner_thread:
            self.runner_thread.stop()
            self.runner_thread.join()
            self.runner_thread = None

    def add_to_outbox(self, message: str):
        msg_bytes = message.encode()
        self.outbox.messages.append(Message(msg_bytes, len(msg_bytes)))

    def clear_outbox(self):
        if self.runner_thread:
            raise Exception('Cannot clear outbox while messages are being sent.')
        self.outbox = MessageBox(CovertNTPServer.MESSAGE_BOX_SIZE)

    def clear_inbox(self):
        if self.runner_thread:
            raise Exception('Cannot clear outbox while messages are being received.')
        self.inbox = MessageBox(CovertNTPServer.MESSAGE_BOX_SIZE)

    def is_sending(self):
        if self.runner_thread:
            return True
        else:
            return False

    def is_receiving(self):
        return self.is_sending()

    def is_running(self):
        return self.is_sending() or self.is_receiving()

    def update_settings(self, settings):
        try:
            self.hostname = settings['hostname']
            self.port = 123
            self.stratum = settings['stratum']
            self.precision = settings['precision']
            self.ref_id = settings['ref-id']
            if self.stratum == 1:
                self.ref_id = list(ReferenceIdentifiers)[self.ref_id]

        except KeyError:
            raise Exception('Make sure "hostname", "stratum", "precision", and "ref-id" are set.')


def main():
    import argparse
    from commands.globals import shared_settings
    from commands.globals import get_interface

    parser = argparse.ArgumentParser(description='Start NTP server')
    parser.add_argument('-n',
                        '--hostname',
                        default='localhost',
                        help='Hostname of NTP server')

    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=123,
                        help='Port of NTP server')

    parser.add_argument('message',
                        help='Message to send to client')

    args = parser.parse_args()

    shared_settings['mode'] = 'client'
    shared_settings['hostname'] = args.hostname
    shared_settings['port'] = args.port
    server = get_interface()

    server.add_to_outbox(args.message)
    messages = [
        'Afraid large celebrated mrs direct.',
        'Equal saw certainty incommode hundred.',
        'Cousin inquietude insipidity full family.'
    ]
    for message in messages:
        server.add_to_outbox(message)

    try:
        server.start()
    except KeyboardInterrupt:
        log_normal('Shutting down...')
    finally:
        server.stop()


if __name__ == '__main__':
    main()
