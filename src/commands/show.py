from tabulate import tabulate

from terminalui.commands import BaseCommand

from .globals import shared_settings


class ShowCmd(BaseCommand):
    """Show global config"""
    name = 'show'
    description = ('Show global config')

    def subparser(self):
        return None

    def __call__(self, argline):
        print(tabulate(
            shared_settings.items(),
            headers=('key', 'value')
        ))

