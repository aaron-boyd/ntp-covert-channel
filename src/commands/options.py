from tabulate import tabulate

from terminalui.commands import BaseCommand

from libNTP.NTP import ReferenceIdentifiers


class OptionsCmd(BaseCommand):
    """Show NTP protocol configuration options"""
    name = 'options'
    description = ('Show NTP protocol configuration options')

    def do_stratum(self, argline):
        """Print stratum options"""
        stratums = {
            '0': 'unspecified or invalid',
            '1': 'primary server(e.g., equipped with a GPS receiver)',
            '2-15': 'secondary server(via NTP)',
            '16': 'unsynchronized',
            '17-255': 'reserved',
        }

        print(tabulate(
            stratums.items(),
            headers=('value', 'description')
        ))

    def do_reference_ids(self, argline):
        """Print Reference Identifier options"""
        ref_ids_dict = dict()

        for i, ref_id in enumerate(ReferenceIdentifiers):
            ref_ids_dict[i] = ref_id.name

        print(tabulate(
            ref_ids_dict.items(),
            headers=('index', 'name')
        ))
