import argparse

from tabulate import tabulate
from terminalui.commands import BaseCommand
from terminalui.tui import log_normal
from terminalui.tui import log_success

from .globals import get_interface
from libNTP.Message import Message


class OutboxCmd(BaseCommand):
    """Interface with Client and Server outbox"""
    name = 'outbox'
    description = ('Interface with Client and Server outbox')

    def do_add(self, argline):
        """Add a message to outbox"""
        parser = argparse.ArgumentParser(description="Add a message to outbox")
        parser.add_argument("message",
                            help="Message to add")

        args = parser.parse_args(argline)
        interface = get_interface()
        interface.add_to_outbox(args.message)
        log_success('Message Added')

    def do_list(self, argline):
        """List messages in outbox"""
        interface = get_interface()
        if not len(interface.outbox):
            log_normal('Outbox empty')
            return

        print(tabulate(
            interface.outbox.messages,
            headers=Message.headers
        ))

    def do_clear(self, argline):
        """Clear messages in outbox"""
        interface = get_interface()
        interface.clear_outbox()
        log_success('Outbox cleared')
