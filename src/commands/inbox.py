from tabulate import tabulate
from terminalui.commands import BaseCommand
from terminalui.tui import log_normal
from terminalui.tui import log_success

from .globals import get_interface
from libNTP.Message import Message


class InboxCmd(BaseCommand):
    """Interface with Client and Server inbox"""
    name = 'inbox'
    description = ('Interface with Client and Server inbox')

    def do_list(self, argline):
        """List messages in inbox"""
        interface = get_interface()
        if not len(interface.inbox):
            log_normal('Inbox empty')
            return

        print(tabulate(
            interface.inbox.messages,
            headers=Message.headers
        ))

    def do_clear(self, argline):
        """Clear messages in inbox"""
        interface = get_interface()
        interface.clear_inbox()
        log_success('Inbox cleared')
