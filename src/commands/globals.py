"""Quick implementation of shared instance vars in the shelli
Modified/accessed directly via set/show commands
"""
from terminalui.tui import log_error

from libNTP.CovertNTPClient import CovertNTPClient
from libNTP.CovertNTPServer import CovertNTPServer

server = CovertNTPServer()
client = CovertNTPClient()
shared_settings = dict()


def shutdown():
    server.shutdown()
    client.shutdown()


def get_interface():
    try:
        mode = shared_settings['mode']
    except KeyError:
        raise Exception('Make sure "mode" setting is set.')

    tmp_dict = {
        'client': client,
        'server': server
    }
    try:
        interface = tmp_dict[mode]
        return interface
    except KeyError:
        raise Exception('Invalid mode {}.'.format(mode))
