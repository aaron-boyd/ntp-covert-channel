from terminalui.commands import BaseCommand
from terminalui.tui import log_error
from terminalui.tui import log_success

from .globals import get_interface
from .globals import shared_settings


class SendCmd(BaseCommand):
    """Send messages"""
    name = 'send'
    description = ('Send message')

    def do_start(self, argline):
        """Start sending messages"""
        interface = get_interface()
        interface.update_settings(shared_settings)
        interface.start_send()
        log_success('Send started')

    def do_stop(self, argline):
        """Stop sending messages"""
        interface = get_interface()
        interface.stop_send()
        log_success('Send stopped')

