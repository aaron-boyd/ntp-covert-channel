from terminalui.commands import BaseCommand
from terminalui.shell import ShellExitException

from .globals import shutdown


class QuitCmd(BaseCommand):
    """Quit Command"""
    name = 'quit'
    description = ('Quit Command')

    def subparser(self):
        return None

    def __call__(self, argline):
        shutdown()
        raise ShellExitException()
