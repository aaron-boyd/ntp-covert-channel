from terminalui.commands import BaseCommand
from terminalui.tui import log_error
from terminalui.tui import log_success

from .globals import get_interface
from .globals import shared_settings


class ReceiveCmd(BaseCommand):
    """Receive messages"""
    name = 'receive'
    description = (__doc__)

    def do_start(self, argline):
        """Start receiving messages"""
        interface = get_interface()
        interface.update_settings(shared_settings)
        interface.start_receive()
        log_success('Receive started')

    def do_stop(self, argline):
        """Stop receiving messages"""
        interface = get_interface()
        interface.stop_receive()
        log_success('Receive stopped')
