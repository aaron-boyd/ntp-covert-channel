import argparse
import re
import struct

from tabulate import tabulate
from terminalui.commands import BaseCommand
from terminalui.tui import log_warn

from .globals import shared_settings
from .globals import client
from .globals import server
from libNTP.NTP import ReferenceIdentifiers


class SetCmd(BaseCommand):
    """Set global config options"""
    name = 'set'
    description = ('Set global config options')

    def subparser(self):
        return None

    def __call__(self, argline):
        parser = argparse.ArgumentParser()
        parser.add_argument('key')
        parser.add_argument('value')
        args = vars(parser.parse_args(argline))

        if client.is_running() or server.is_running():
            log_warn('Client/Server must be restarted for changes to take effect!')

        # simple int cast
        try:
            args['value'] = int(args['value'])
        except Exception:
            pass

        # Special set values for NTP
        if args['key'] == 'stratum':
            args['value'] = self.validate_stratum(args['value'])
        elif args['key'] == 'poll':
            args['value'] = self.validate_poll(args['value'])
        elif args['key'] == 'precision':
            args['value'] = self.validate_precision(args['value'])
        elif args['key'] == 'ref-id':
            args['value'] = self.validate_ref_id(args['value'])

        shared_settings[args['key']] = args['value']

        print(tabulate(
            shared_settings.items(),
            headers=('key', 'value')
        ))

    def validate_stratum(self, value):
        """Validate NTP stratum configuration option (only matters to server)"""
        # Stratum
        # 0 = unspecified or invalid
        # 1 = primary server(e.g., equipped with a GPS receiver)
        # 2-15 = secondary server(via NTP)
        # 16 = unsynchronized
        # 17-255 = reserved
        if 0 <= value < 17:
            if 'ref-id' in shared_settings:
                log_warn('Unsetting "ref-id" because it depends on "stratum".')
                shared_settings.pop('ref-id')
            return value
        else:
            raise ValueError('Invalid stratum value')

    def validate_poll(self, value):
        """Validate NTP poll configuration option"""
        # Poll: 8-bit signed integer representing the maximum interval between successive messages,
        # in log2 seconds. Suggested default limits for minimum and maximum poll intervals are 6 and 10, respectively.
        if value < 0:
            raise ValueError('Invalid poll value')
        elif value < 6 or value > 10:
            log_warn('Suggested poll value is between 6 and 10.')
        return value

    def validate_ref_id(self, value):
        """Validate NTP reference id configuration option (only matters to server)"""
        # Reference ID Above stratum 1 (secondary servers and clients): this is the reference identifier of the
        # server and can be used to detect timing loops. If using the IPv4 address family, the identifier is the
        # four-octet IPv4 address.If using the IPv6 address family, it is the first four octets of the MD5 hash of
        # the IPv6 address.

        try:
            stratum = shared_settings['stratum']
        except KeyError:
            raise Exception('Set "stratum first"')

        if stratum == 0:  # KISS Code
            value = str(value)
            if len(value) > 4:
                raise OverflowError('KISS code must be no longer than 4 bytes.')
            value = value.encode() + b'\x00' * (4 - len(value))
            value = int(value.hex(), 16)
            return value
        elif stratum == 1:  # Assigned string
            for indx, ref_id in enumerate(ReferenceIdentifiers):
                if value == indx:
                    return indx
            raise ValueError('Invalid ReferenceIdentifiers index (Use options command).')
        else:  # IP
            valid_ip_regex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
            str_value = str(value)
            if not re.match(valid_ip_regex, str_value):
                raise ValueError('Invalid IP address')
            oct_1, oct_2, oct_3, oct_4 = [int(num) for num in value.split('.')]
            packed_bytes = struct.pack('BBBB', oct_4, oct_3, oct_2, oct_1)
            value = struct.unpack('I', packed_bytes)[0]
            return value

    def validate_precision(self, value):
        """Validate NTP precision configuration option (only matters to server)"""
        # Precision: 8-bit signed integer representing the precision of the system clock, in log2 seconds. For
        # instance, a value of -18 corresponds to a precision of about one microsecond. The precision can be
        # determined when the service first starts up as the minimum time of several iterations to read the system
        # clock.
        return int(value)
