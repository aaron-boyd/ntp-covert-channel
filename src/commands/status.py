from tabulate import tabulate

from terminalui.commands import BaseCommand

from .globals import client
from .globals import server


class StatusCmd(BaseCommand):
    """Show NTP interface status"""
    name = 'status'
    description = ('Show NTP interface status')

    def subparser(self):
        return None

    def __call__(self, argline):
        statuses = {
            'client sending': client.is_sending(),
            'client receiving': client.is_receiving(),
            'server sending': server.is_sending(),
            'server receiving': server.is_receiving(),
        }

        print(tabulate(
            statuses.items(),
            headers=('task', 'running')
        ))

