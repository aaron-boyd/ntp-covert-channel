# NTP Covert Channel

## Install
```
sudo python3 setup.py install
```

## Running Client
The following parameters must be set to run the client and can be set with the `set` command.

| Parameter  | Description                            | Example                  |
| ---------- | -------------------------------------- | ------------------------ |
| `mode`     | Mode to run in                         | `set mode client`        |
| `hostname` | Hostname of server                     | `set hostname localhost` |
| `port`     | Port on server                         | `set port 123`           |
| `poll`     | Interval to send packets (2^n seconds) | `set poll 6`             |

## Running Server
The following parameters must be set to run the client and can be set with the `set` command.

**NOTE: Server must be run with root privileges because it listens on port 123.

| Parameter   | Description                             | Example                  |
| ----------- | --------------------------------------- | ------------------------ |
| `mode`      | Mode to run in                          | `set mode server`        |
| `hostname`  | Hostname to listen on                   | `set hostname localhost` |
| `precision` | Clock precision (2^n seconds)           | `set precision -12`      |
| `stratum`   | Stratum value (`options stratum`)       | `set stratum 1`          |
| `ref-id`    | Reference ID  (`options reference_ids`) | `set ref-id 4`           |

## Reference Identifiers
Reference identifiers depend on the servers stratum value. Valid reference ids for the following stratum values are as follows:

| Stratum | Reference ID Type                            | Example                  |
| ------- | -------------------------------------------- | ------------------------ |
| 0       | KISS code (4-byte string)                    | `set ref-id ABCD`        |
| 1       | Reference ID index (`options reference_ids`) | `set ref-id 4`           |
| 2-16    | IP of source clock server                    | `set ref-id 192.168.0.1` |

## Example Sending Message
### Client
```
$ ./src/covert-ntp-shell.py

     ___
    / |,\    ____      __  _  _____ _____ 
   |- ' -|  / (__` == |  \| ||_   _|| ()_)
    \_,_/   \____)    |_|\__|  |_|  |_|
    /___\

c-ntp $ set mode client
key    value
-----  -------
mode   client
c-ntp $ set hostname localhost
key       value
--------  ---------
mode      client
hostname  localhost
c-ntp $ set port 123
key       value
--------  ---------
mode      client
hostname  localhost
port      123
c-ntp $ set poll 6
key       value
--------  ---------
mode      client
hostname  localhost
port      123
poll      6
c-ntp $ outbox add "Hello, Server!"
 [success] Message Added
c-ntp $ send start
 [success] Send started
c-ntp $ outbox list
status        size    expected size  message
----------  ------  ---------------  --------------
PROCESSING      14               14  Hello, Server!
```

### Server
```
$ sudo ./src/covert-ntp-shell.py

     ___
    / |,\    ____      __  _  _____ _____ 
   |- ' -|  / (__` == |  \| ||_   _|| ()_)
    \_,_/   \____)    |_|\__|  |_|  |_|
    /___\

c-ntp $ set mode server
key    value
-----  -------
mode   server
c-ntp $ set hostname localhost
key       value
--------  ---------
mode      server
hostname  localhost
c-ntp $ set precision -12
key        value
---------  ---------
mode       server
hostname   localhost
precision  -12
c-ntp $ set stratum 2
key        value
---------  ---------
mode       server
hostname   localhost
precision  -12
stratum    2
c-ntp $ set ref-id 192.168.4.23
key        value
---------  ----------
mode       server
hostname   localhost
precision  -12
stratum    2
ref-id     3232236567
c-ntp $ outbox add "Hello, Client!"
 [success] Message Added
c-ntp $ send start
 [success] Send started
c-ntp $ outbox list
status      size    expected size  message
--------  ------  ---------------  --------------
INIT          14               14  Hello, Client!
```
